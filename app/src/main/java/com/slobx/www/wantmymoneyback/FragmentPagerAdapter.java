package com.slobx.www.wantmymoneyback;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by BCA on 8/29/2017.
 */

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    private final int NUMBER_OF_PAGES = 3;
    private final String[] tabTitles = {"Who owe me", "Who I owe", "Statistics"};

    public FragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        WhoOweMeFragment whoOweMeFragment = null;

        switch (position){
            case 0:
                whoOweMeFragment = new WhoOweMeFragment();
                break;
            case 1:
                whoOweMeFragment = new WhoOweMeFragment();
                break;
            case 2:
                whoOweMeFragment = new WhoOweMeFragment();
                break;
        }

        return whoOweMeFragment;
    }

    @Override
    public int getCount() {
        return NUMBER_OF_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
