package com.slobx.www.wantmymoneyback;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

public class DebtActivity extends FragmentActivity {

    TextView username;
    String sentUserName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt);

        sentUserName = getIntent().getStringExtra("name");

        username = findViewById(R.id.username);



        if (sentUserName != null){
            username.setText("Welcome " + sentUserName + "!");
        } else {
            username.setText("Welcome guest!");
        }

        FragmentPagerAdapter adapterViewPager;

        ViewPager vpPager = (ViewPager) findViewById(R.id.pager);
        adapterViewPager = new FragmentPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
    }
}
