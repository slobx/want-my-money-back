package com.slobx.www.wantmymoneyback;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class WhoOweMeListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final Integer[] imgId;
    private final String[] name;
    private final String[] amount;
    private final String[] date;

    public WhoOweMeListAdapter(Activity context, Integer[] imgId, String[] name, String[] amount, String[] date) {
        super(context, R.layout.who_ome_me_listview_item, name);
        this.context = context;
        this.imgId = imgId;
        this.name = name;
        this.amount = amount;
        this.date = date;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.who_ome_me_listview_item, null, true);

        ImageView iconImageView = rowView.findViewById(R.id.icon);
        TextView nameText = rowView.findViewById(R.id.name_who_owe_me);
        TextView amountText = rowView.findViewById(R.id.amount_who_owe_me);
        TextView deadlineTextTitle = rowView.findViewById(R.id.deadline_title_who_owe_me);
        TextView deadlineTextTime = rowView.findViewById(R.id.deadline_time_who_owe_me);

        iconImageView.setImageResource(imgId[position]);
        nameText.setText(name[position]);
        amountText.setText(amount[position]);
        deadlineTextTitle.setText("Deadline");
        deadlineTextTime.setText(date[position]);

        return rowView;
    }
}
