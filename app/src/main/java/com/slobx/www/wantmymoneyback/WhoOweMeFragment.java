package com.slobx.www.wantmymoneyback;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


public class WhoOweMeFragment extends android.support.v4.app.Fragment {

    ListView listView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.who_owe_me_fragment, container, false);


        //TO-DO here you need to implement loading from MySql logic
        final String[] names = {
                "Person 1",
                "Person 2",
                "Person 3",
                "Person 4",
                "Person 5",
                "Person 6",
                "Person 7",
                "Person 8",
                "Person 9",
                "Person 10"
        };

        //TO-DO here you need to implement loading from MySql logic
        final String[] amounts = {
                "10.000 RSD",
                "250 €",
                "500 €",
                "402.050 RSD",
                "1.250 €",
                "10.000 RSD",
                "10.000 RSD",
                "A10.000 RSD",
                "10.000 RSD",
                "10.000 RSD"
        };

        //TO-DO here you need to implement loading from MySql logic
        final String[] deadlineDates = {
                "12.09.2017",
                "13.09.2017",
                "25.09.2017",
                "30.10.2017",
                "31.12.2017",
                "31.12.2017",
                "31.12.2017",
                "31.12.2017",
                "31.12.2017",
                "31.12.2017"
        };

        //TO-DO here you need to implement loading from MySql logic
        final Integer[] imgid = {
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher,
        };

        WhoOweMeListAdapter adapter = new WhoOweMeListAdapter(getActivity(),imgid,names,amounts,deadlineDates);
        listView = view.findViewById(R.id.list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(getActivity(), "You clicked on "+ names[position], Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
